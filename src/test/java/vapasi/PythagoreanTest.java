package vapasi;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class PythagoreanTest {
    @Test
    public void shouldReturnPythagoreanTripletForPositiveNumber() {
        int number=1000;
        String strExpected="200,375,425";
        String strActual = PythagoreanTriplet.getTripletNaturalNumbers(number);
        assertThat(strActual,is(strExpected));
    }

    @Test
    public void shouldReturnNullForNegativeNumber() {
        int number=-1000;
        String strExpected=null;
        String strActual = PythagoreanTriplet.getTripletNaturalNumbers(number);
        assertThat(strActual,is(strExpected));
    }

    @Test
    public void shouldReturnNullForZeroNumber() {
        int number=0;
        String strExpected=null;
        String strActual = PythagoreanTriplet.getTripletNaturalNumbers(number);
        assertThat(strActual,is(strExpected));
    }
}