package vapasi;

public class PythagoreanTriplet {
    public static String getTripletNaturalNumbers(int num) {
        for (int a = 1; a <= num / 3; a++) {
            for (int b = a + 1; b <= num / 2; b++) {
                int c = num - a - b;
                if ((a * a) + (b * b) == (c * c)) {
                    return a + "," + b + "," + c;
                }
            }
        }
        return null;
    }
}
