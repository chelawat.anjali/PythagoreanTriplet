package vapasi;

public class PythagoreanClientProgram {
    public static void main(String[] args) {
        int number = 1000;
        System.out.println("Pythagorean Triplets for " + number +
                            " are:" + PythagoreanTriplet.getTripletNaturalNumbers(number));
    }
}